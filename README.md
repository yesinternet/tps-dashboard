#How to setup Members plugin

Before the activation of Members plugin, deactivate the tps-roles plugin

## Create the following custom roles

* product_contributor (Product Contributor)
* product_reviewer (Product Reviewer)

## Assign the following capabilities

###Product Contributor

* read
* upload_files
* edit_posts
* delete_posts
* edit_product
* read_product
* delete_product
* edit_products
* delete_products
* manage_product_terms
* edit_product_terms
* delete_product_terms
* assign_product_terms

###Product Reviewer
* delete_others_posts
* edit_others_posts
* edit_others_products
* delete_others_products
* publish_products
* read_private_products
* delete_private_products
* edit_private_products
* edit_published_products
* delete_published_products