<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://theparentsshop.com
 * @since      1.0.0
 *
 * @package    Tps_Dashboard
 * @subpackage Tps_Dashboard/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Tps_Dashboard
 * @subpackage Tps_Dashboard/includes
 * @author     Haris Kaklamanos <ck@theparentsshop.com>
 */
class Tps_Dashboard {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Tps_Dashboard_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

		$this->plugin_name = 'tps-dashboard';
		$this->version = '1.0.1';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Tps_Dashboard_Loader. Orchestrates the hooks of the plugin.
	 * - Tps_Dashboard_i18n. Defines internationalization functionality.
	 * - Tps_Dashboard_Admin. Defines all hooks for the admin area.
	 * - Tps_Dashboard_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		* The class responsible for dashboard tips
		*/
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-tps-dashboard-tips.php';

		/**
		* The class responsible for product category promo text meta
		*/
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/term-meta/class-tps-term-meta-product-cat-promo-text.php';

		/**
		* The class responsible for product category visibility meta
		*/
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/term-meta/class-tps-term-meta-product-cat-visibility.php';

		/**
		* The class responsible for adding product promo positions
		*/
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/submit-box/class-tps-submit-box-product-visibility.php';

		/**
		* The class responsible for adding post source url meta box
		*/
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/meta-boxes/class-tps-meta-box-post-source.php';

		/**
		* The class responsible for adding promo text meta box
		*/
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/meta-boxes/class-tps-meta-box-promo-text.php';

		/**
		* The class responsible for adding product promo meta box
		*/
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/meta-boxes/class-tps-meta-box-product-promo-image.php';

		/**
		* The class responsible for user sex meta
		*/
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/user-meta/class-tps-user-meta-sex.php';

		/**
		* The class responsible for users, roles, capabilitis etc
		*/
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-tps-dashboard-users.php';

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-tps-dashboard-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-tps-dashboard-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-tps-dashboard-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-tps-dashboard-public.php';

		$this->loader = new Tps_Dashboard_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Tps_Dashboard_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Tps_Dashboard_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Tps_Dashboard_Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );

		$this->loader->add_action( 'wp_before_admin_bar_render', $plugin_admin , 'remove_wp_logo_from_admin' , 0 );
		$this->loader->add_action( 'admin_head', $plugin_admin , 'remove_help_tabs' , 0 );
		$this->loader->add_action( 'wp_dashboard_setup', $plugin_admin , 'remove_dashboard_widgets' , 0 );
		$this->loader->add_action( 'admin_menu', $plugin_admin , 'remove_wp_version_from_footer' , 0 );

		//Remove product attributes submenu
		$this->loader->add_action( 'admin_menu', $plugin_admin , 'remove_product_attributes_submenu' , 999 );
		$this->loader->add_action( 'admin_menu', $plugin_admin , 'remove_posts_menu' , 999 );
		$this->loader->add_action( 'admin_menu', $plugin_admin , 'remove_tools_menu' , 999 );

		//Remove product meta boxes
		$this->loader->add_action( 'admin_menu', $plugin_admin , 'remove_product_custom_fields_metabox' , 0 );
		$this->loader->add_action( 'admin_menu', $plugin_admin , 'remove_product_slugdiv_metabox' , 0 );
		$this->loader->add_action( 'add_meta_boxes', $plugin_admin , 'remove_product_commentsdiv_metabox' , 99);

		//Add product category promo text meta
		$this->loader->add_action( 'product_cat_edit_form_fields', 'Tps_Term_Meta_Product_Cat_Promo_Text' , 'render' ) ;
		$this->loader->add_action( 'edit_product_cat', 'Tps_Term_Meta_Product_Cat_Promo_Text' , 'save' ) ;

		//Add product category visibility meta
		$this->loader->add_action( 'product_cat_edit_form_fields', 'Tps_Term_Meta_Product_Cat_Visibility' , 'render' ) ;
		$this->loader->add_action( 'edit_product_cat', 'Tps_Term_Meta_Product_Cat_Visibility' , 'save' ) ;

		//Add product visibility settings inside the submit box
		$this->loader->add_action( 'post_submitbox_misc_actions', 'Tps_Submit_Box_Product_Visibility' , 'render' ) ;
		$this->loader->add_action( 'save_post', 'Tps_Submit_Box_Product_Visibility' , 'save' ,  10, 2 ) ;

		//Add post source url metabox in post edit screen
		$this->loader->add_action( 'add_meta_boxes', 'Tps_Meta_Box_Post_Source' , 'add' ) ;
		$this->loader->add_action( 'save_post', 'Tps_Meta_Box_Post_Source' , 'save' ,  10, 2 ) ;

		//Add promo text metabox in pages and products
		$this->loader->add_action( 'add_meta_boxes', 'Tps_Meta_Box_Promo_Text' , 'add' ) ;
		$this->loader->add_action( 'save_post', 'Tps_Meta_Box_Promo_Text' , 'save' ,  10, 2 ) ;

		//Add promo image metabox in products
		$this->loader->add_action( 'add_meta_boxes_product', 'Tps_Meta_Box_Product_Promo_Image' , 'add' ) ;
		$this->loader->add_action( 'save_post', 'Tps_Meta_Box_Product_Promo_Image' , 'save' ,  10, 2 ) ;

		//Add author support for products
		$this->loader->add_action('init' , $plugin_admin , 'add_author_to_products', 999 );

		//Add excerpt support for pages
		$this->loader->add_action('init' , $plugin_admin , 'add_excerpt_to_pages', 10 );

		remove_action( 'admin_color_scheme_picker', 'admin_color_scheme_picker' );

		$this->loader->add_filter( 'woocommerce_product_data_tabs', $plugin_admin, 'remove_wc_product_tabs' , 10 , 1 );
		$this->loader->add_filter( 'product_type_selector', $plugin_admin, 'remove_wc_product_types' , 10 , 1 );

		//Apply filters responsible for dashboard tips
		$this->loader->add_filter( 'admin_post_thumbnail_html', 'Tps_Dashboard_Tips', 'admin_post_thumbnail_html' , 10 , 1 );
		$this->loader->add_action( 'tps_product_promo_image_size_tip', 'Tps_Dashboard_Tips', 'tps_product_promo_image_size_tip' );
		$this->loader->add_action( 'product_cat_edit_form_fields', 'Tps_Dashboard_Tips', 'tps_product_cat_image_size_tip' , 999);

		//Show and save user meta
		$this->loader->add_action( 'show_user_profile', 'Tps_User_Meta_Sex', 'render_backend' );
		$this->loader->add_action( 'edit_user_profile', 'Tps_User_Meta_Sex', 'render_backend' );
		$this->loader->add_action( 'woocommerce_edit_account_form', 'Tps_User_Meta_Sex', 'render_frontend' );
		
		$this->loader->add_action( 'personal_options_update', 'Tps_User_Meta_Sex', 'profile_update' );
		$this->loader->add_action( 'edit_user_profile_update', 'Tps_User_Meta_Sex', 'profile_update' );
		
		$this->loader->add_action( 'woocommerce_save_account_details_errors', 'Tps_User_Meta_Sex', 'save_account_details' , 99 , 2 );

		//Change default role for registered users to subscriber
		$this->loader->add_filter( 'woocommerce_new_customer_data', 'Tps_Dashboard_Users', 'change_default_customer_role' );

		//Change default password strength to 1
		$this->loader->add_filter( 'woocommerce_min_password_strength', 'Tps_Dashboard_Users', 'min_password_strength' );

		//Change default password hint
		$this->loader->add_filter( 'gettext', 'Tps_Dashboard_Users', 'change_i18n_password_hint' , 10 , 3 );

		//Remove Wordpress version meta tag from <head>
		remove_action('wp_head', 'wp_generator');
	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Tps_Dashboard_Public( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Tps_Dashboard_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}


