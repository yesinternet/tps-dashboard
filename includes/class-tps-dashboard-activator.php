<?php

/**
 * Fired during plugin activation
 *
 * @link       http://theparentsshop.com
 * @since      1.0.0
 *
 * @package    Tps_Dashboard
 * @subpackage Tps_Dashboard/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Tps_Dashboard
 * @subpackage Tps_Dashboard/includes
 * @author     Haris Kaklamanos <ck@theparentsshop.com>
 */
class Tps_Dashboard_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
