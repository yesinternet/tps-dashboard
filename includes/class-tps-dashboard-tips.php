<?php

/**
 * Class responsible for dashboard tips.
 *
 */
class Tps_Dashboard_Tips {

	/**
	 * Post thumbnail-featured image size tip
	 */
	public static function admin_post_thumbnail_html( $content ) {

		$screen = get_current_screen();

		if ( isset ( $screen->post_type ) && $screen->post_type == 'product')
		{
		
			$content.='<p>';
			$content.= self::image_tip_header() ;
		
			$content.= __( '<strong>Ratio:</strong> 1.5 (3:2)<br/><strong>Recommended Size (px):</strong> 1440 x 960<br/><strong>Min.Size (px):</strong> 720 x 480' , 'tps-dashboard' );

			$content.='</span></p>';
		}

		if ( isset ( $screen->post_type ) && $screen->post_type == 'page')
		{
			$content.= self::full_screen_image_tip();
		}

		return $content;

	}

	public static function tps_product_promo_image_size_tip()
	{

		echo self::full_screen_image_tip();
	}

	public static function tps_product_cat_image_size_tip($arg)
	{
	?>
		<tr class="form-field form-required term-imaze-size-tip">
			<th scope="row">
				<label></label>
			</th>
			<td>
				<?php echo self::full_screen_image_tip();?>
			</td>
		</tr>

	<?php
	}

	private static function full_screen_image_tip()
	{
		$content = '' ;

		$content.='<p>';
		
		$content.=self::image_tip_header();
		
		$content.= __( '<strong>Ratio:</strong> 1.77 (16:9)<br/><strong>Recommended Size (px):</strong> 1920 x 1080<br/><strong>Other sizes (px):</strong> 1680 x 945 , 1366 x 768' , 'tps-dashboard' ) ;

		$content.='</p>';

		return $content;	
	}

	private static function image_tip_header()
	{

		$content='<span><i style="width:20px;height:20px;background:#00a695;color:#fff;padding:3px 6px;margin-right:5px;">P</i><strong>Ratio & Size Tip</strong><hr>';

		return $content;	
	}

}
