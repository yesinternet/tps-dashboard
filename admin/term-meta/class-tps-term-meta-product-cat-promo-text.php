<?php
/**
 * Product Category Promo Text Meta 
 *
 * Displays the promo text meta field for product category
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Tps_Term_Meta_Product_Cat_Promo_Text Class.
 */
class Tps_Term_Meta_Product_Cat_Promo_Text {

   /**
	* The HTML for the promo text term meta
	*
	*/
	static function render( $term ) {

		$product_cat_promo_text = get_term_meta( $term->term_id, '_tps_term_meta_product_cat_promo_text', true );
	
	?>

		<tr class="form-field form-required term-promo-text-wrap">
			<th scope="row">
				<label for="_tps_term_meta_product_cat_promo_text"><?php _e( 'Promo Text', 'tps-dashboard' )?></label>
			</th>
			<td>
				<?php wp_nonce_field( basename( __FILE__ ), '_tps_term_meta_product_cat_promo_text_nonce' ); ?>
				<input name="_tps_term_meta_product_cat_promo_text" id="product_cat_promo_text" type="text" value="<?php echo $product_cat_promo_text;?>" size="40" aria-required="true">
				<p class="description"><?php _e('Add a promo text for the category','tps-dashboard');?></p>
			</td>
		</tr>

	<?php 

	}

   /**
	* Save promo text term meta
	* If empty, then delete the entry from the database
	* 
	*/
	function save( $term_id ) {

		if ( ! isset( $_POST['_tps_term_meta_product_cat_promo_text_nonce'] ) || ! wp_verify_nonce( $_POST['_tps_term_meta_product_cat_promo_text_nonce'], basename( __FILE__ ) ) )
        	return;

		$old_promo_text = get_term_meta( $term_id, '_tps_term_meta_product_cat_promo_text', true );
    	$new_promo_text = isset( $_POST['_tps_term_meta_product_cat_promo_text'] ) ? sanitize_text_field( $_POST['_tps_term_meta_product_cat_promo_text'] ) : '';

    	if ( $old_promo_text && '' === $new_promo_text )
    	{
        	delete_term_meta( $term_id, '_tps_term_meta_product_cat_promo_text' );
        }
   		else if ( $old_promo_text !== $new_promo_text )
   		{
        	update_term_meta( $term_id, '_tps_term_meta_product_cat_promo_text', $new_promo_text );
        }
		

	}

}