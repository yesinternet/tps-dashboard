<?php
/**
 * Product Visibility Submit Box Actions
 *
 * Displays the product visibility setting inside the submit box
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Tps_Submit_Box_Product_Visibility Class.
 */
class Tps_Submit_Box_Product_Visibility {

   /**
	* The HTML for the product visibility settings
	*
	*/
	static function render( $post ) {

		if ( $post->post_type != "product" )
		{
			return;
		}

		wp_nonce_field( basename( __FILE__ ), '_tps_product_visibility_nonce' ); 

		$homepage_primary_slider = get_post_meta( $post->ID, '_tps_homepage_primary_slider', true ) ;
		$homepage_secondary_slider = get_post_meta( $post->ID, '_tps_homepage_secondary_slider', true ) ;
		$product_cat_slider = get_post_meta( $post->ID, '_tps_product_cat_slider', true ) ;
		
		?>

		<div class="misc-pub-section" id="tps-product-visibility">

			<span>
				<i style="width:20px;height:20px;background:#00a695;color:#fff;padding:3px 6px;margin-right:5px;">P</i>
				<?php _e( 'Product Visibility', 'tps-dashboard' ) ;?>
			</span> 
			<?php if ( $homepage_primary_slider == 'yes') echo '<strong>['.__( 'Homepage Primary Slider', 'tps-dashboard' ).']</strong>' ;?>
			<?php if ( $homepage_secondary_slider == 'yes') echo '<strong>['.__( 'Homepage Secondary Slider', 'tps-dashboard' ).']</strong>' ;?>
			<?php if ( $product_cat_slider == 'yes') echo '<strong>['.__( 'Category Page Slider', 'tps-dashboard' ).']</strong>' ;?>

				
			<a href="#tps-product-visibility" class="edit-tps-product-visibility hide-if-no-js"><?php _e( 'Edit', 'tps-dashboard' ); ?></a>
		
			<div id="tps-product-visibility-select" class="hide-if-js">

				<p>
					<input type="checkbox" name="_tps_homepage_primary_slider" id="_tps_homepage_primary_slider" <?php if ( $homepage_primary_slider == 'yes') echo 'checked="checked"' ;?>/>
					<label for="_tps_homepage_primary_slider"><?php _e( 'Homepage Primary Slider', 'tps-dashboard' ) ;?></label>
				</p>

				<p>
					<input type="checkbox" name="_tps_homepage_secondary_slider" id="_tps_homepage_secondary_slider" <?php if ( $homepage_secondary_slider == 'yes') echo 'checked="checked"' ;?>/>
					<label for="_tps_homepage_secondary_slider"><?php _e( 'Homepage Secondary Slider', 'tps-dashboard' ) ;?></label>
				</p>

				<p>
					<input type="checkbox" name="_tps_product_cat_slider" id="_tps_product_cat_slider" <?php if ( $product_cat_slider == 'yes') echo 'checked="checked"' ;?>/>
					<label for="_tps_product_cat_slider"><?php _e( 'Category Page Slider', 'tps-dashboard' ) ;?></label>
				</p>

				<a href="#tps-product-visibility" class="cancel-tps-product-visibility hide-if-no-js"><?php _e( 'Cancel', 'tps-dashboard' ); ?></a>
					
			</div>	

		</div>
	<?php 

	}

   /**
	* Save product visibility setttings
	*
	*/
	static function save( $post_id ) {

		global $post;
		
		// Verify nonce
		if ( !isset( $_POST['_tps_product_visibility_nonce'] ) || !wp_verify_nonce( $_POST['_tps_product_visibility_nonce'], basename(__FILE__) ) ) {
			return $post_id;
		}
		
		// Check Autosave
		if ( (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) || ( defined('DOING_AJAX') && DOING_AJAX) || isset($_REQUEST['bulk_edit']) ) {
			return $post_id;
		}

		// Don't save if only a revision
		if ( isset( $post->post_type ) && $post->post_type == 'revision' ) {
			return $post_id;
		}

		// Check permissions
		if ( !current_user_can( 'edit_product', $post->ID ) ) {
			return $post_id;
		}

		//Homepage primary slider
		$homepage_primary_slider = $_POST['_tps_homepage_primary_slider'];

		if ( isset ( $homepage_primary_slider ) )
		{
			update_post_meta( $post->ID, '_tps_homepage_primary_slider', 'yes' );
		}
		else
		{
			delete_post_meta( $post->ID, '_tps_homepage_primary_slider' );
		}

		//Homepage secondary slider
		$homepage_secondary_slider = $_POST['_tps_homepage_secondary_slider'];

		if ( isset ( $homepage_secondary_slider ) )
		{
			update_post_meta( $post->ID, '_tps_homepage_secondary_slider', 'yes' );
		}
		else
		{
			delete_post_meta( $post->ID, '_tps_homepage_secondary_slider' );
		}

		//Category page product slider
		$product_cat_slider = $_POST['_tps_product_cat_slider'];

		if ( isset ( $product_cat_slider ) )
		{
			update_post_meta( $post->ID, '_tps_product_cat_slider', 'yes' );
		}
		else
		{
			delete_post_meta( $post->ID, '_tps_product_cat_slider' );
		}
		
	}

}