<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://theparentsshop.com
 * @since      1.0.0
 *
 * @package    Tps_Dashboard
 * @subpackage Tps_Dashboard/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Tps_Dashboard
 * @subpackage Tps_Dashboard/admin
 * @author     Haris Kaklamanos <ck@theparentsshop.com>
 */
class Tps_Dashboard_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Tps_Dashboard_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Tps_Dashboard_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/tps-dashboard-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {


		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Tps_Dashboard_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Tps_Dashboard_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/tps-dashboard-admin.js', array( 'jquery' ), $this->version, false );

		global $typenow;
	    
	    if( $typenow == 'product' ) 
	    {
	        wp_enqueue_media();
	 
	        // Registers and enqueues the required javascript.
	        wp_register_script( 'tps-dashboard-meta-box-image', plugin_dir_url( __FILE__ ) . 'js/tps-dashboard-meta-box-product-promo-image.js', array( 'jquery' ) , $this->version, false );
	        wp_localize_script( 'tps-dashboard-meta-box-image', 'meta_image',
	            array(
	                'title' => __( 'Choose or Upload an Image', 'tps-dashboard' ),
	                'button' => __( 'Use this as Promo Image', 'tps-dashboard' ),
	            )
	        );
	       
	        wp_enqueue_script( 'tps-dashboard-meta-box-image' );


	        wp_register_script( 'tps-dashboard-meta-box-product-visibility', plugin_dir_url( __FILE__ ) . 'js/tps-dashboard-meta-box-product-visibility.js', array( 'jquery' ) , $this->version, false );
	        wp_enqueue_script( 'tps-dashboard-meta-box-product-visibility' );
	    }

	}

	/**
	 * Remove WP logo from admin bar.
	 *
	 * @since    1.0.0
	 */
	public function remove_wp_logo_from_admin() {
	
		if ( is_super_admin () ){

			return;
		}
	    
	    global $wp_admin_bar;

	    $wp_admin_bar->remove_menu('wp-logo');
	}


	/**
	 * Remove WP logo from admin bar.
	 *
	 * @since    1.0.0
	 */
	public function remove_dashboard_widgets() {
	
		if ( is_super_admin () ){

			return;
		}
	    
	    global $wp_meta_boxes;

	    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
	    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']);
	    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
	    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
	    
		remove_meta_box( 'wpseo-dashboard-overview', 'dashboard', 'side' );
	    
	    
	}


	/**
	 * Remove WP logo from admin bar.
	 *
	 * @since    1.0.0
	 */
	public function remove_wp_version_from_footer() {
	
		if ( is_super_admin () ){

			return;
		}
	    
	    remove_filter( 'update_footer', 'core_update_footer' ); 
	}


	/**
	 * Remove Posts Menu for non-admins
	 *
	 * @since    1.0.0
	 */
	public function remove_posts_menu() {
	
		if ( is_super_admin () ){

			return;
		}
	    
	     remove_menu_page( 'edit.php' );                   //Posts
	}

	/**
	 * Remove Tools Menu for non-admins
	 *
	 * @since    1.0.0
	 */
	public function remove_tools_menu() {
	
		if ( is_super_admin () ){

			return;
		}
	    
	     remove_menu_page( 'tools.php' );                   //Tools
	}

	/**
	 * Remove product attributes submenu from admin menu.
	 *
	 * @since    1.0.0
	 */
	public function remove_product_attributes_submenu() {
	
		if ( is_super_admin () ){

			return;
		}
	    
	     remove_submenu_page( 'edit.php?post_type=product', 'product_attributes' );
	}


	/**
	 * Remove Custom Fields metabox
	 *
	 * @since    1.0.0
	 */
	public function remove_product_custom_fields_metabox() {
	
		if ( is_super_admin () ){

			return;
		}
	    
		remove_meta_box( 'postcustom' , 'product' , 'normal' ); 
	}

	/**
	 * Remove Slug metabox
	 *
	 * @since    1.0.0
	 */
	public function remove_product_slugdiv_metabox() {
	
		if ( is_super_admin () ){

			return;
		}
	    
		remove_meta_box( 'slugdiv' , 'product' , 'normal' ); 
	}

	/**
	 * Remove Comments metabox
	 *
	 * @since    1.0.0
	 */
	public function remove_product_commentsdiv_metabox() {
	
		if ( is_super_admin () ){

			return;
		}
	    
		remove_meta_box( 'commentsdiv' , 'product' , 'normal' ); 
	}


	/**
	 * Add author to products
	 *
	 * @since    1.0.0
	 */
	function add_author_to_products() {

	   add_post_type_support( 'product', 'author' );
	}

	/**
	 * Add excerpt to pages
	 */
	function add_excerpt_to_pages() {
	   add_post_type_support( 'page', 'excerpt' );
	}

	/**
	 * Remove WP help tabs.
	 *
	 * @since    1.0.0
	 */
	public function remove_help_tabs() {

		if ( is_super_admin () ){

			return;
		}

	    $screen = get_current_screen();
   		$screen->remove_help_tabs();
	}

	/**
	 * Remove WC product tabs
	 *
	 * @since    1.0.0
	 */
	public function remove_wc_product_tabs( $tabs ) {

		if ( is_super_admin () ){

			return $tabs;
		}

	    unset( $tabs['shipping'] );
	    unset( $tabs['inventory'] );
	    unset( $tabs['linked_product'] );
	    unset( $tabs['advanced'] );
	    unset( $tabs['attribute'] );

		return $tabs;
	}

	/**
	 * Remove WC product types
	 *
	 * @since    1.0.0
	 */
	function remove_wc_product_types( $types ){
 
 		if ( is_super_admin () ){

			return $types;
		}

    	unset( $types['grouped'] );
    	unset( $types['simple'] );
    	unset( $types['variable'] );
    	//unset( $types['external'] );

    	return $types;
	}

}
