<?php

/**
 * This class defines the functions to change default WC customer roles
 */
class Tps_Dashboard_Users {

	/**
	 * Change default role for registered users
	 *
	 */
	public static function change_default_customer_role($new_user) {
		
		$new_user['role'] = 'subscriber';
		
		return $new_user;
	}

	/**
	 * Change default password strength to 1
	 *
	 */
	public static function min_password_strength( $password_strength ) {
		
		return 1;
	}

	/**
	 * Change default password hint
	 *
	 */
	public static function change_i18n_password_hint( $translation, $text, $domain ) {

		if ( $domain == 'woocommerce' && $text == 'The password should be at least seven characters long. To make it stronger, use upper and lower case letters, numbers and symbols like ! " ? $ % ^ &amp; ).' ){
			$translation = __( 'Please use upper & lower case letters, numbers and symbols.' , 'tps-dashboard' );
		}

		return $translation;
	}

}
