<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://theparentsshop.com
 * @since      1.0.0
 *
 * @package    Tps_Dashboard
 * @subpackage Tps_Dashboard/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Tps_Dashboard
 * @subpackage Tps_Dashboard/includes
 * @author     Haris Kaklamanos <ck@theparentsshop.com>
 */
class Tps_Dashboard_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
