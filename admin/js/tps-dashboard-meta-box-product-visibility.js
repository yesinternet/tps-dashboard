(function( $ ) {
	'use strict';

	jQuery(document).ready(function($){
	 
		// TPS product visibility.
		$( '#tps-product-visibility' ).find( '.edit-tps-product-visibility' ).click( function() {
			
			if ( $( '#tps-product-visibility-select' ).is( ':hidden' ) ) {
				$( '#tps-product-visibility-select' ).slideDown( 'fast' );
				$( this ).hide();
			}
			return false;
		});

		$( '#tps-product-visibility' ).find( '.cancel-tps-product-visibility' ).click( function() {
			
			$( '#tps-product-visibility-select' ).slideUp( 'fast' );

			$( '#tps-product-visibility' ).find( '.edit-tps-product-visibility' ).show();
			
			return false;
		});
	
	});

})( jQuery );