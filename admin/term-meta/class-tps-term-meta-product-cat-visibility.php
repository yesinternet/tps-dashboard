<?php
/**
 * Product Category Visibility Meta 
 *
 * Displays the product category visibility settings meta
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Tps_Term_Meta_Product_Cat_Visibility Class.
 */
class Tps_Term_Meta_Product_Cat_Visibility {

   /**
	* The HTML for the visibility term meta
	*
	*/
	static function render( $term ) {

		$homepage_product_cat_slider = get_term_meta( $term->term_id, '_tps_homepage_product_cat_slider', true );
	
	?>

		<tr class="form-field form-required term-visibility-wrap">
			<th scope="row">
				<label for="_tps_homepage_product_cat_slider"><?php _e( 'Visibility', 'tps-dashboard' )?></label>
			</th>
			<td>
				<?php wp_nonce_field( basename( __FILE__ ), '_tps_homepage_product_cat_slider_nonce' ); ?>
				<input type="checkbox" name="_tps_homepage_product_cat_slider" id="_tps_homepage_product_cat_slider" <?php if ( $homepage_product_cat_slider == 'yes') echo 'checked="checked"' ;?>/>
				<label for="_tps_homepage_product_cat_slider"><?php _e( 'Homepage Category Slider', 'tps-dashboard' ) ;?></label>
				<p class="description"><?php _e('Show category image in homepage category slider','tps-dashboard');?></p>
			</td>
		</tr>

	<?php 

	}

   /**
	* Save visibility term meta
	* If empty, then delete the entry from the database
	* 
	*/
	function save( $term_id ) {

		if ( ! isset( $_POST['_tps_homepage_product_cat_slider_nonce'] ) || ! wp_verify_nonce( $_POST['_tps_homepage_product_cat_slider_nonce'], basename( __FILE__ ) ) )
        	return;

		//Homepage category slider
		$homepage_product_cat_slider = $_POST['_tps_homepage_product_cat_slider'];

		if ( isset ( $homepage_product_cat_slider ) )
		{
        	update_term_meta( $term_id, '_tps_homepage_product_cat_slider', 'yes' );
		}
		else
		{
        	delete_term_meta( $term_id, '_tps_homepage_product_cat_slider' );
		}

	}

}