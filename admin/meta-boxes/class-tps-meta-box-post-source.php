<?php
/**
 * Post Source Meta Box
 *
 * Displays the source meta box for posts that are copied from other sources.
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Tps_Meta_Box_Post_Source Class.
 */
class Tps_Meta_Box_Post_Source {

	/**
	 * Register the post source metabox to be used for posts
	 *
	 */
	static function add() {
		
		add_meta_box(
			'tps_meta_box_post_source',
			__('Source URL' , 'tps-dashboard'),
			array( 'Tps_Meta_Box_Post_Source' ,  'render' ),
			array ( 'post' ),
			'normal',
			'high'
		);
	}

   /**
	* The HTML for the post source meta box
	*
	*/
	static function render( $post ) {

		$post_source = get_post_meta( $post->ID, '_tps_post_source', true );

		wp_nonce_field( basename( __FILE__ ), '_tps_post_source_nonce' ); 

	?>

	<p>
		<label class="screen-reader-text" for="_tps_post_source"><?php _e( 'Source URL', 'tps-dashboard' )?></label>
	</p>
	<p>
		<input id="_tps_post_source" name="_tps_post_source" class="large-text" placeholder="<?php _e('URL of the source webpage, i.e. http://www.webpage.com/link','tps-dashboard');?>" value="<?php echo $post_source;?>" type="text">
		<a href="javascript:void(0);" onclick="jQuery('#_tps_post_source').val('');"><?php _e( 'Clear', 'tps-dashboard' ) ;?></a>

	</p>	


	<?php 

	}

   /**
	* Save post source meta
	*
	*/
	static function save( $post_id ) {

		global $post;
		
		// Verify nonce
		if ( !isset( $_POST['_tps_post_source_nonce'] ) || !wp_verify_nonce( $_POST['_tps_post_source_nonce'], basename(__FILE__) ) ) {
			return $post_id;
		}
		
		// Check Autosave
		if ( (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) || ( defined('DOING_AJAX') && DOING_AJAX) || isset($_REQUEST['bulk_edit']) ) {
			return $post_id;
		}

		// Don't save if only a revision
		if ( isset( $post->post_type ) && $post->post_type == 'revision' ) {
			return $post_id;
		}

		// Check permissions
		if ( !current_user_can( 'edit_post', $post->ID ) ) {
			return $post_id;
		}

		$post_source = esc_url( $_POST['_tps_post_source'] , array ( 'http', 'https' ) );

		if ( empty ( $post_source ) )
		{
			delete_post_meta( $post->ID, '_tps_post_source' );
		}
		else
		{
			update_post_meta( $post->ID, '_tps_post_source', $post_source );
		}

	}

}