(function( $ ) {
	'use strict';

	/*
	 * Attaches the image uploader to the input field
	 */
	jQuery(document).ready(function($){
	 
	    // Instantiates the variable that holds the media library frame.
	    var product_promo_image_frame;
 
	    // Runs when the image button is clicked.
	    $('#tps_meta_box_product_promo_image').on ( 'click', '.set_product_promo_image_link' , function(e){

	        // Prevents the default action from occuring.
	        e.preventDefault();
	 
	        // If the frame already exists, re-open it.
	        if ( product_promo_image_frame ) {
	            product_promo_image_frame.open();
	            return;
	        }
	 
	        // Sets up the media library frame
	        product_promo_image_frame = wp.media({
	            title: meta_image.title,
           	 	button: { text:  meta_image.button },
            	library: { type: 'image' },
            	multiple: false
	        });


			product_promo_image_frame.on('open',function() {
				  
				  var selection = product_promo_image_frame.state().get('selection');
				  
				  var attachment = wp.media.attachment( $('#product_promo_image_id').val() );
				  
				  attachment.fetch();

				  selection.add( attachment ? [ attachment ] : [] );

			});

 
	        // Runs when an image is selected.
	        product_promo_image_frame.on('select', function(){
	 
	            // Grabs the attachment selection and creates a JSON representation of the model.
	            var selection = product_promo_image_frame.state().get('selection').first().toJSON();

	            // Sends the attachment URL to our custom image input field.
	            $('#product_promo_image_id').val(selection.id);
	            
	            $('#product_promo_image_preview').html('<a class="set_product_promo_image_link" href="#"><img width="266" height="266" src="'+selection.sizes.medium.url+'" class="img-responsive" alt="'+selection.title+'" style= "max-width:100%;border: 1px solid #d5d5d5;"></a>');
	        	
	        	$('.product_promo_image_link').toggleClass('hidden');

	        });
	 
	        // Opens the media library frame.
	        product_promo_image_frame.open();
	    });

		$('#tps_meta_box_product_promo_image').on ( 'click', '.remove_product_promo_image_link' , function(e){
	        
	        // Prevents the default action from occuring.
	        e.preventDefault();
	        $('#product_promo_image_preview').html('');
	        $('#product_promo_image_id').val('');
	        $('.product_promo_image_link').toggleClass('hidden');


		});
	
	});

})( jQuery );