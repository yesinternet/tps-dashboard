<?php
/**
 * Sex User Meta
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Tps_User_Meta_Sex Class.
 */
class Tps_User_Meta_Sex {

	/**
	* The HTML for the user meta fields
	*
	*/
	public static function render_backend( $user ) {

		$sex = get_user_meta( $user->ID , 'sex', true  ) ;

	?>
	
	<h3><?php _e( 'Extra Profile Information' , 'tps-dashboard' );?></h3>

	<table class="form-table">

		<tr>
			<th><label for="sex">Sex</label></th>

			<td>

				<fieldset>
					<legend class="screen-reader-text"><span><?php _e( 'Sex' , 'tps-dashboard' );?></span></legend>
					<label>
						<input name="sex" value="male" type="radio" <?php if ( $sex == "male" ) echo 'checked="checked"';?>> <span><?php _e( 'Male' , 'tps-dashboard' );?></span>
					</label>
					<br>
					<label>
						<input name="sex" value="female" type="radio" <?php if ( $sex == "female" ) echo 'checked="checked"';?>> <span><?php _e( 'FeMale' , 'tps-dashboard' );?></span>
					</label>
				</fieldset>

			</td>
		</tr>

	</table>
	
	<?php 

	}

	public static function render_frontend( ) {

		$user = wp_get_current_user();
		$sex = get_user_meta( $user->ID , 'sex', true );

	?>

	<fieldset>
		<legend><?php _e( 'Gender', 'woocommerce' ); ?></legend>

        <div class="form-group">
            <div class="radio-inline">
                <label>
                    <input type="radio" class="woocommerce-Input" name="sex" id="sex" value="male" <?php if ( $sex == 'male' ) echo 'checked="checked"' ;?>/>
                    <span><?php _e( 'Male', 'tps-dahsboard' ); ?></span>
                </label>
            </div>
            <div class="radio-inline">
                <label>
                    <input type="radio" class="woocommerce-Input" name="sex" id="sex" value="female" <?php if ( $sex == 'female' ) echo 'checked="checked"' ;?>/>
                    <span><?php _e( 'Female', 'tps-dahsboard' ); ?></span>
                </label>
            </div>
        </div>

	</fieldset>
	<div class="clear"></div>

	
	<?php 

	}

	public static function profile_update( $user_id  ) {
		
		if ( !current_user_can( 'edit_user', $user_id ) )
			return false;

		if( !isset( $_POST['sex'] ) )
			return false;
		
		update_user_meta( $user_id, 'sex', $_POST['sex'] );
	
	}

	public static function save_account_details( $errors , $user  ) {

		// Do not continue if there is already any error
		if ( wc_notice_count( 'error' ) !== 0 ) {
			return;
		}
		
		if ( $errors->get_error_messages() ) {
			return;
		}
		
		if ( !current_user_can( 'edit_user', $user -> ID ) ){
			$errors -> add( 'tps_update_gender_permission_error' , __('You are not allowed to edit your gender information.' , 'tps-newsletter') );
			return false;
		}

		$sex = sanitize_text_field ( $_POST['sex'] );

		if ( $sex != get_user_meta( $user -> ID , 'sex', true ) ){

			$update_user_meta = update_user_meta( $user -> ID , 'sex', sanitize_text_field ( $_POST['sex'] ) );
			
			if ( !$update_user_meta ){
				$errors -> add( 'tps_update_gender_error' , __('There was a problem trying to update your gender. Please try again.' , 'tps-newsletter') );
			}
		}
	
	}

}