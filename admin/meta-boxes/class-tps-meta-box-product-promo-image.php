<?php
/**
 * Product Promo Image Meta Box
 *
 * Displays the promo image meta box for products.
 * Promo data are displayed in sliders, automated emails etc
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Tps_Meta_Box_Product_Promo_Image Class.
 */
class Tps_Meta_Box_Product_Promo_Image {

	/**
	 * Register the promo metabox to be used for the product post type
	 *
	 */
	static function add() {
		
		add_meta_box(
			'tps_meta_box_product_promo_image',
			__('Product Promo Image' , 'tps-dashboard'),
			array( 'Tps_Meta_Box_Product_Promo_Image', 'render' ),
			'product',
			'side',
			'low'
		);
	}

   /**
	* The HTML for the promo meta box
	*
	*/
	static function render( $post ) {


		$product_promo_image_id = get_post_meta( $post->ID, '_tps_product_promo_image_id', true );

		wp_nonce_field( basename( __FILE__ ), '_tps_product_promo_image_id_nonce' ); 

	?>
	
	<p id="product_promo_image_preview" class="hide-if-no-js">
	
	<?php if ( !empty( $product_promo_image_id ) ) :?>
	
		  <a class="set_product_promo_image_link" href="#">

		  	<?php echo wp_get_attachment_image( $product_promo_image_id ,  array( 266, 266 )  , false , array ( 'style' => 'max-width:100%;border: 1px solid #d5d5d5;') ) ;?>

		  </a>

	<?php endif;?>
	
	</p>
	
	<p class="hide-if-no-js product_promo_image_link <?php if ( !empty( $product_promo_image_id ) ) echo 'hidden';?>">
		<a id="set_product_promo_image_link" class="set_product_promo_image_link" href="#"><?php _e( 'Choose or Upload a Promo Image', 'tps-dashboard' )?></a>
	</p>


	<p class="hide-if-no-js product_promo_image_link <?php if ( empty( $product_promo_image_id ) ) echo 'hidden';?>">
		<a id="remove_product_promo_image_link" class="remove_product_promo_image_link" href="#"><?php _e( 'Remove Promo Image', 'tps-dashboard' )?></a>
	</p>


	<?php do_action ('tps_product_promo_image_size_tip') ;?>		

	<input type="hidden" name="_tps_product_promo_image_id" id="product_promo_image_id" value="<?php echo $product_promo_image_id; ?>" />


	<?php 

	}

   /**
	* Save promo metabox
	*
	* @since 0.1.0
	*/
	static function save( $post_id ) {

		global $post;
		
		// Verify nonce
		if ( !isset( $_POST['_tps_product_promo_image_id_nonce'] ) || !wp_verify_nonce( $_POST['_tps_product_promo_image_id_nonce'], basename(__FILE__) ) ) {
			return $post_id;
		}
		
		// Check Autosave
		if ( (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) || ( defined('DOING_AJAX') && DOING_AJAX) || isset($_REQUEST['bulk_edit']) ) {
			return $post_id;
		}

		// Don't save if only a revision
		if ( isset( $post->post_type ) && $post->post_type == 'revision' ) {
			return $post_id;
		}

		// Check permissions
		if ( !current_user_can( 'edit_product', $post->ID ) ) {
			return $post_id;
		}

		$product_promo_image = absint (  $_POST['_tps_product_promo_image_id'] );

		if ( empty ( $product_promo_image ) )
		{
			 delete_post_meta( $post->ID, '_tps_product_promo_image_id' );
		}
		else
		{
			update_post_meta( $post->ID, '_tps_product_promo_image_id', $product_promo_image );
		}
	}




}