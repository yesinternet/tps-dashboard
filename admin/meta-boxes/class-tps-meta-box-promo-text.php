<?php
/**
 * Promo Text Meta Box
 *
 * Displays the promo text meta box for products and pages.
 * This data is displayed in sliders, full screen images, automated emails etc
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Tps_Meta_Box_Promo_Text Class.
 */
class Tps_Meta_Box_Promo_Text {

	/**
	 * Register the promo text metabox to be used for the pages & products
	 *
	 */
	static function add() {
		
		add_meta_box(
			'tps_meta_box_promo_text',
			__('Promo Text' , 'tps-dashboard'),
			array( 'Tps_Meta_Box_Promo_Text' ,  'render' ),
			array ( 'page' , 'product' ),
			'normal',
			'high'
		);
	}

   /**
	* The HTML for the promo text meta box
	*
	*/
	static function render( $post ) {

		$promo_text = get_post_meta( $post->ID, '_tps_promo_text', true );

		wp_nonce_field( basename( __FILE__ ), '_tps_promo_text_nonce' ); 

	?>

	<p>
		<label class="screen-reader-text" for="_tps_promo_text"><?php _e( 'Promo Text', 'tps-dashboard' )?></label>
	</p>
	<p>
		<input id="_tps_promo_text" name="_tps_promo_text" class="large-text" placeholder="<?php _e('Add a promo text for sliders, intro images, emails etc','tps-dashboard');?>" value="<?php echo $promo_text;?>" type="text">
	</p>	


	<?php 

	}

   /**
	* Save promo metabox
	*
	* @since 0.1.0
	*/
	static function save( $post_id ) {

		global $post;
		
		// Verify nonce
		if ( !isset( $_POST['_tps_promo_text_nonce'] ) || !wp_verify_nonce( $_POST['_tps_promo_text_nonce'], basename(__FILE__) ) ) {
			return $post_id;
		}
		
		// Check Autosave
		if ( (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) || ( defined('DOING_AJAX') && DOING_AJAX) || isset($_REQUEST['bulk_edit']) ) {
			return $post_id;
		}

		// Don't save if only a revision
		if ( isset( $post->post_type ) && $post->post_type == 'revision' ) {
			return $post_id;
		}

		// Check permissions
		if ( !current_user_can( 'edit_product', $post->ID ) ) {
			return $post_id;
		}

		$promo_text = sanitize_text_field(  $_POST['_tps_promo_text'] );

		if ( empty ( $promo_text ) )
		{
			 delete_post_meta( $post->ID, '_tps_promo_text' );
		}
		else
		{
			update_post_meta( $post->ID, '_tps_promo_text', $promo_text );
		}

	}

}